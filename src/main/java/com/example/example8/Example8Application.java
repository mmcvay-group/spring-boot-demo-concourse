package com.example.example8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Example8Application {

	//private String name;


	public static void main(String[] args) {
		SpringApplication.run(Example8Application.class, args);
	}

	@GetMapping("/")
	String home() {
		return "Changing text!";
	}

	@GetMapping("/hello")
	String hello() {
		return "Hello World!";
	}	

}
